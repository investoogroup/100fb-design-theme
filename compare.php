<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./dist/global.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.min.css">
  <title>100FB Design</title>
</head>

<body>
  <div class="app">
    <header class="">
      <?php include './components/stats.php' ?>
      <?php include './modules/nav.php' ?>
      <?php include './modules/hero.php' ?>
      <?php include './modules/breadcrumbs.php' ?>
    </header>
    <main>
      <?php include './modules/compare-hub.php' ?>
      <?php include './components/table.php' ?>
      <?php include './components/wysiwyg.php' ?>
      <?php include './modules/cta-banner.php' ?>
      <?php include './components/accordian.php' ?>
    </main>
  </div>
  <?php include './modules/footer.php' ?>
  <script src="./dist/global.js"></script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./dist/global.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.min.css">
  <title>100FB Design</title>
</head>

<body>
  <div class="app">
    <header class="">
      <?php include './components/stats.php' ?>
      <?php include './modules/nav.php' ?>
    </header>
    <main>
      <?php include './modules/hero-single.php' ?>
      <?php include './modules/partners.php' ?>
      <?php include './modules/news-hub.php' ?>
    </main>
  </div>
  <?php include './modules/footer.php' ?>
  <script src="./dist/global.js"></script>
</body>

</html>
<article class="article-teaser">
  <div class="article-teaser__content">
    <p class="article-teaser__excerpt">
      <a href="" class="link link--turns-alt-color">Eos sunt ipsam natus  molestias non optio eveniet iste, repellat et quos...
    </p>
    <div class="article-teaser__tags">
      <a href="#" class="link link--turns-alt-color article-teaser__tag">
        <div class="article-teaser__tag-icon"></div>
        <div class="article-teaser__tag-text article-teaser__tag-text--has-icon">01 - March - 2000</div>
      </a>
      <a href="#" class="link link--turns-alt-color article-teaser__tag">
        <div class="article-teaser__tag-icon"></div>
        <div class="article-teaser__tag-text article-teaser__tag-text--has-icon">Forex</div>
      </a>
    </div>
  </div>
</article>
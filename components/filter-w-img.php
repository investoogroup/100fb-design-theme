<section class="filter-w-img">
  <a href="#" class="filter-w-img__wrap">
    <div class="filter-w-img__main">
      <img src="./assets/media/img/paypal-clear.png" alt="" class="filter-w-img__logo">
    </div>
    <div class="filter-w-img__button">
      <div class="filter-w-img__link">PayPal</div>
    </div>
  </a>
</section>
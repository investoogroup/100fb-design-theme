<section class="module">
  <div class="container">
    <h2 class="module__title">Forex Broker FAQs:</h2>
    <ul class="accordian">
      <li data-accordian-item class="accordian__item">
        <h3 data-accordian-header class="accordian__header">Eaque accusantium ducimus eveniet, vto ut ipsa accusamus?</h3>
        <div data-accordian-body class="accordian__body">
          <div class="accordian__main">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis accusantium magnam beatae deserunt est perspiciatis repellendus! Animi consequatur iusto facilis libero eum officia quisquam, a culpa, soluta aliquam inventore qui!</div>
        </div>
      </li>
      <li data-accordian-item class="accordian__item">
        <h3 data-accordian-header class="accordian__header">Ducimus eveniet, vto ut accusamus eaque accusantium ipsa?</h3>
        <div data-accordian-body class="accordian__body">
          <div class="accordian__main">
            <div>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis accusantium magnam beatae deserunt est perspiciatis repellendus! Animi consequatur iusto facilis libero eum officia quisquam, a culpa, soluta aliquam inventore qui!</div>
            <div>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis accusantium magnam beatae deserunt est perspiciatis repellendus! Animi consequatur iusto facilis libero eum officia quisquam, a culpa, soluta aliquam inventore qui!</div>
          </div>
        </div>
      </li>
      <li data-accordian-item class="accordian__item">
        <h3 data-accordian-header class="accordian__header">Accusamus eveniet accusantium ducimus vto ut?</h3>
        <div data-accordian-body class="accordian__body">
          <div class="accordian__main">
            <div>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis accusantium magnam beatae deserunt est perspiciatis repellendus! Animi consequatur iusto facilis libero eum officia quisquam, a culpa, soluta aliquam inventore qui!</div>
            <div>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis accusantium magnam beatae deserunt est perspiciatis repellendus! Animi consequatur iusto facilis libero eum officia quisquam, a culpa, soluta aliquam inventore qui!</div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</section>
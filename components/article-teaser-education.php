<article class="article-teaser">
  <div class="article-teaser__image" style="background-image:url(./assets/media/img/fluentcity.png)">
    <div class="article-teaser__label article-teaser__label--a">Beginner</div>
  </div>
  <h3 class="article-teaser__title article-teaser__title--small">
    <a href="#" class="link link--turns-alt-color">Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio ab nesciunt delectus.</a>
  </h3>
  <div class="article-teaser__content">
    <div class="article-teaser__tags">
      <a href="#" class="link link--turns-alt-color article-teaser__tag">
        <div class="article-teaser__tag-text">Jay Stuart</div>
      </a>
    </div>
  </div>
</article>
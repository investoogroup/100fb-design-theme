<div class="stats">
  <div class="container container--wide">
    <div class="stats__wrapper">
      <div class="stats__item">
        <div class="stats__header">
          <div class="stats__header-name">S&P 500</div>
          <div class="stats__header-figure">3252.7</div>
        </div>
        <div class="stats__figure">
          <span class="stats__figure--main">
            <div class="stats__arrow"></div>
            0.24%
          </span>
          <span class="stats__figure--secondary">7.9</span>
        </div>
      </div>
      <div class="stats__item">
        <div class="stats__header">
          <div class="stats__header-name">Nasdaq 100</div>
          <div class="stats__header-figure">8981.6</div>
        </div>
        <div class="stats__figure">
          <span class="stats__figure--main">
            <div class="stats__arrow"></div>
            0.29%
          </span>
          <span class="stats__figure--secondary">25.7</span>
        </div>
      </div>
      <div class="stats__item">
        <div class="stats__header">
          <div class="stats__header-name">EUR/USD</div>
          <div class="stats__header-figure">1.10216</div>
        </div>
        <div class="stats__figure">
          <span class="stats__figure--main">
            <div class="stats__arrow"></div>
            0.03%
          </span>
          <span class="stats__figure--secondary">0.000035</span>
        </div>
      </div>
      <div class="stats__item">
        <div class="stats__header">
          <div class="stats__header-name">BTC/USD</div>
          <div class="stats__header-figure">9088.00</div>
        </div>
        <div class="stats__figure">
          <span class="stats__figure--main">
            <div class="stats__arrow"></div>
            2.18%
          </span>
          <span class="stats__figure--secondary">193.46</span>
        </div>
      </div>
      <div class="stats__item">
        <div class="stats__header">
          <div class="stats__header-name">ETH/USD</div>
          <div class="stats__header-figure">172.81</div>
        </div>
        <div class="stats__figure">
          <span class="stats__figure--main">
            <div class="stats__arrow"></div>
            1.81%
          </span>
          <span class="stats__figure--secondary">3.8</span>
        </div>
      </div>
    </div>
  </div>
</div>
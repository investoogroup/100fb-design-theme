<article class="article-teaser has-image">
  <div class="article-teaser__image" style="background-image:url(./assets/media/img/money.png)"></div>
  <div class="article-teaser__content">
    <h3 class="article-teaser__title">
      <a href="#" class="link link--turns-alt-color">Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio ab nesciunt delectus.</a>
    </h3>
    <div class="article-teaser__tags">
      <a href="#" class="link link--turns-alt-color article-teaser__tag">
        <div class="article-teaser__tag-icon"></div>
        <div class="article-teaser__tag-text article-teaser__tag-text--has-icon">01 - March - 2000</div>
      </a>
      <a href="#" class="link link--turns-alt-color article-teaser__tag">
        <div class="article-teaser__tag-icon"></div>
        <div class="article-teaser__tag-text article-teaser__tag-text--has-icon">Forex</div>
      </a>
    </div>
    <p class="article-teaser__excerpt">Eos sunt ipsam natus labore, dolorum error, quidem veritatis sed omnis eius. Accusantium molestias non optio eveniet iste, repellat et quos...</p>
  </div>
</article>
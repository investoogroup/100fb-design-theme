<section class="focused">
  <div class="focused__row">
    <h3 class="focused__title">Focued Copy Section</h3>
  </div>
  <div class="focused__row wysiwyg">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor massa id neque aliquam vestibulum. Sed pulvinar proin gravida hendrerit lectus a. Sem fringilla ut morbi tincidunt augue interdum. Sed risus ultricies tristique nulla aliquet enim tortor. Vel orci porta non pulvinar neque laoreet suspendisse interdum. Dui nunc mattis enim ut. In ante metus dictum at.</p>
    <p>Et dolore magna aliqua. Porttitor massa id neque aliquam vestibulum. Sed pulvinar proin gravida hendrerit lectus a. Sem fringilla ut morbi tincidunt augue interdum. Sed risus ultricies tristique nulla aliquet enim tortor. Vel orci porta non pulvinar neque laoreet suspendisse interdum. Dui nunc mattis enim ut. In ante metus dictum at.</p>
    <p>Sit amet dictum sit amet justo donec enim diam. Augue ut lectus arcu bibendum at varius vel pharetra. Nisi quis eleifend quam adipiscing vitae proin sagittis. Pulvinar etiam non quam lacus suspendisse faucibus interdum posuere lorem. Nibh nisl condimentum id venenatis. Nunc pulvinar sapien et ligula. Pharetra magna ac placerat vestibulum lectus mauris ultrices eros. Eget velit aliquet sagittis id consectetur purus. Convallis a cras semper auctor neque vitae.</p>
  </div>
  <div class="focused__row focused__btn">
    <a href="#" class="btn btn--theme-b btn--has-arrow">Take me there</a>
  </div>
</section>
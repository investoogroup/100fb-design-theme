const navElement = document.getElementById("nav");
const navLinkElements = document.querySelectorAll("[data-nav-link]");
const overlay = document.getElementById("overlay");
const switchClass = (id, classAdd, classRemove) => {
  let add = id.classList.add(classAdd);
  let remove = id.classList.remove(classRemove);
  return { add, remove };
};
class Megamenu {
  constructor(nav, overlay) {
    this.nav = nav;
    this.overlay = overlay;
  }

  getMenu(link) {
    this.link = link;
    this.menu = link.nextElementSibling;
  }
  show() {
    switchClass(this.link, "nav__link--active", "nav__link--inactive");
    switchClass(this.menu, "megamenu--active", "megamenu--inactive");
    switchClass(this.overlay, "overlay--active", "overlay--inactive");
    switchClass(this.nav, "nav--dropdown-active", "nav--dropdown-inactive");
  }
  hide() {
    switchClass(this.link, "nav__link--inactive", "nav__link--active");
    switchClass(this.menu, "megamenu--inactive", "megamenu--active");
    switchClass(this.overlay, "overlay--inactive", "overlay--active");
    switchClass(this.nav, "nav--dropdown-inactive", "nav--dropdown-active");
  }
}

const megamenu = new Megamenu(navElement, overlay);
navLinkElements.forEach(link => {
  link.addEventListener("mouseover", () => {
    console.log("hello");
    megamenu.getMenu(link);
    megamenu.show();
  });
  link.addEventListener("mouseout", () => {
    megamenu.getMenu(link);
    megamenu.hide();
  });
  if (link.nextElementSibling) {
    link.nextElementSibling.addEventListener("mouseover", () => {
      megamenu.getMenu(link);
      megamenu.show();
    });
    link.nextElementSibling.addEventListener("mouseout", () => {
      megamenu.getMenu(link);
      megamenu.hide();
    });
  }
});

const items = document.querySelectorAll("[data-accordian-item]");
const headers = document.querySelectorAll("[data-accordian-header]");
const bodies = document.querySelectorAll("[data-accordian-body]");

class Accordian {
  show(header, body, item) {
    header.classList.add("accordian__header--active");
    body.style.maxHeight = "500px";
    item.classList.add("accordian__item--active");
  }

  hide(header, body, item) {
    header.classList.remove("accordian__header--active");
    body.style.maxHeight = "0px";
    item.classList.remove("accordian__item--active");
  }
}

const accordian = new Accordian();

headers.forEach((header, index) => {
  let itemHeight = items[index].getBoundingClientRect().x;
  header.addEventListener("click", () => {
    if (items[index].classList.contains("accordian__item--active")) {
      accordian.hide(header, bodies[index], items[index]);
    } else {
      accordian.show(
        header,
        bodies[index],
        items[index],
        items[index].getBoundingClientRect().x
      );
    }
  });
});

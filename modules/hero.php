<div class="hero">
  <div class="container container--wide">
    <div class="row">
      <div class="col-lg-6">
        <div class="hero__text-wrap">
          <h1 class="hero__title">Compare the best Forex Brokers in 2020</h1>
          <h2 class="hero__sub-title">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus cupiditate quod doloribus quas delectus facilis autem labore a cumque.</h2>
          <div class="hero__btn">
            <a href="#" class="btn btn--theme-a">Compare Brokers</a>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="hero__visual">
          <img class="hero__image" src="./assets/media/img/devices.png" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<section class="cta-banner">
  <div class="container">
    <div class="row align-items-center justify-content-between">
      <div class="col-lg-8 cta-banner__header">
        <img src=" ./assets/media/img/handheart.png" alt="" class="cta-banner__icon">
        <div class="cta-banner__title">
          Find & compare the best forex brokers today!
        </div>
      </div>
      <div class="col-lg-3 cta-banner__btn">
        <a href="#" class="btn btn--theme-c btn--has-arrow">Compare Now</a>
      </div>
    </div>
  </div>
</section>
<section class="module">
  <div class="container container--wide">
    <h2 class="module__title module__title--hub">Find & Compare Broker by:</h2>
    <div class="row">
      <div class="module__col-2-5">
        <section class="widget">
          <div class="widget__container">
            <div class="widget__row">
              <a href="#" class="widget__link widget__header">
                <img src="./assets/media/img/creditcard.png" alt="" class="widget__icon">
                <h3 class="widget__title widget__title--compare-by">Payment Methods</h3>
              </a>
            </div>
            <div class="widget__row">
              <ul class="widget__list">
                <li class="widget__list-item"><a href="#" class="widget__link widget__link--colored-hover widget__link--has-arrow">Bank Wire / Transfer</a></li>
                <li class="widget__list-item"><a href="#" class="widget__link widget__link--colored-hover widget__link--has-arrow">PayPal</a></li>
                <li class="widget__list-item"><a href="#" class="widget__link widget__link--colored-hover widget__link--has-arrow">Skrill</a></li>
                <li class="widget__list-item"><a href="#" class="widget__link widget__link--colored-hover widget__link--has-arrow">Credit Card</a></li>
                <li class="widget__list-item"><a href="#" class="widget__link widget__link--colored-hover widget__link--has-arrow">Debit Card</a></li>
              </ul>
            </div>
            <div class="widget__row">
              <a href="" class="widget__link widget__link--colored-hover widget__link--view-all">View all...</a>
            </div>
          </div>
        </section>
      </div>
      

    </div>
  </div>
</section>
<section class="module">
  <div class="container container--wide">
    <?php include './components/banner-h.php' ?>
    <div class="module__has-sidebar">
      <div class="module__main">
        <div class="module__section">
          <div class="module__row">
            <div class="col col-lg-6">
              <h2 class="module__title module__title--hub">Forex Analysis:</h2>
              <?php include './components/article-teaser-w-img.php' ?>
              <?php include './components/article-teaser.php' ?>
              <?php include './components/article-teaser.php' ?>
              <div class="module__read-more">
                <a href="#" class="link link--view-all link--turns-alt-color">Read More...</a>
              </div>
            </div>
            <div class="col col-lg-6">
              <h2 class="module__title module__title--hub">Forex News:</h2>
              <?php include './components/article-teaser.php' ?>
              <?php include './components/article-teaser.php' ?>
              <?php include './components/article-teaser.php' ?>
              <div class="module__read-more">
                <a href="#" class="link link--view-all link--turns-alt-color">Read More...</a>
              </div>
            </div>
          </div>
        </div>
        <div class="module__section">
          <h2 class="module__title module__title--hub">Forex Education:</h2>
          <div class="module__row">
            <div class="col col-lg-4">
              <?php include './components/article-teaser-education.php' ?>
            </div>
            <div class="col col-lg-4">
              <?php include './components/article-teaser-education.php' ?>
            </div>
            <div class="col col-lg-4">
              <?php include './components/article-teaser-education.php' ?>
            </div>
            <div class="col col-lg-4">
              <?php include './components/article-teaser-education.php' ?>
            </div>
            <div class="col col-lg-4">
              <?php include './components/article-teaser-education.php' ?>
            </div>
            <div class="col col-lg-4">
              <?php include './components/article-teaser-education.php' ?>
            </div>
          </div>
          <div class="module__read-more">
            <a href="#" class="link link--view-all">View All...</a>
          </div>
        </div>
      </div>
      <div class="module__sidebar module__sidebar--right">
        <section class="banner-v">
          <img src="./assets/media/img/banner-v.png" alt="" class="">
        </section>
        <h3 class="module__sidebar-title">
          Top Links
        </h3>
        <?php include './components/toplinks.php' ?>
        <h3 class="module__sidebar-title">Recent Articles</h3>
        <?php include './components/article-teaser-sidebar.php' ?>
        <?php include './components/article-teaser-sidebar.php' ?>
        <?php include './components/article-teaser-sidebar.php' ?>
        <?php include './components/article-teaser-sidebar.php' ?>
      </div>
    </div>
  </div>
</section>
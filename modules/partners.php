<section class="module">
  <div class="container container--wide">
    <?php include './components/banner-h.php' ?>
    <h2 class="module__title module__title--hub">Brokers we recommend:</h2>
    <div class="row">
      <div class="module__col-2-5">
        <section class="widget">
          <div class="widget__container">
            <div class="widget__row">
              <img src="assets/media/img/partner.png" alt="" class="widget__featured-image">
            </div>
            <div class="widget__row">
              <a href="#" class="widget__link">
                <h3 class="widget__title">PayPal</h3>
              </a>
            </div>
            <div class="widget__row">
              <div class="row align-items-center">
                <img src="./assets/media/img/star.png" alt="" class="widget__icon widget__icon--partner">
                <img src="./assets/media/img/star.png" alt="" class="widget__icon widget__icon--partner">
                <img src="./assets/media/img/star.png" alt="" class="widget__icon widget__icon--partner">
                <img src="./assets/media/img/star.png" alt="" class="widget__icon widget__icon--partner">
              </div>
            </div>
            <div class="widget__row">
              <div class="row">
                <div class="col col-sm-5">
                  <a href="#" class="btn">Review</a>
                </div>
                <div class="col col-sm-7">
                  <a href="#" class="btn btn--theme-a">Open Account</a>
                </div>
              </div>
            </div>
            <div class="widget__row">
              <div class="widget__disclaimer">71% of retial CFD accounts lose money</div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <a href="#" class="link link--view-all">
      view all...
    </a>
  </div>
</section>
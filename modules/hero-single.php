<div class="hero">
  <div class="container container--wide">
    <div class="hero__text-wrap hero__text-wrap--centered">
      <h1 class="hero__title hero__title--centered">Compare the best Forex Brokers in 2020</h1>
      <h2 class="hero__sub-title">Compare brokers by:</h2>
      <div class="row justify-content-center">
        <div class="hero__btn-col">
          <button class="btn">Payment Methods</button>
        </div>
        <div class="hero__btn-col">
          <button class="btn">Regulations</button>
        </div>
        <div class="hero__btn-col">
          <button class="btn btn--theme-a">Trading Conditions</button>
        </div>
        <div class="hero__btn-col">
          <button class="btn">Countries</button>
        </div>
        <div class="hero__btn-col">
          <button class="btn">Platforms</button>
        </div>
      </div>
    </div>
  </div>
</div>
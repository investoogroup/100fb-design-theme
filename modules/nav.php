<nav id="nav" class="nav nav--dropdown-inactive">
  <div class="container container--wide">
    <div class="nav__wrapper">
      <div class="nav__logo">
        <img class="nav__logo" src="./assets/media/logo.png">
      </div>

      <ul class="nav__main">
        <li class="nav__item"><a  href="/" data-nav-link class="nav__link nav__link--inactive">Home</li></a>
        <li class="nav__item">
          <a href="/compare.php" data-nav-item data-nav-link class="nav__link nav__link--inactive">Compare Brokers</a>
          <ul class="megamenu megamenu--inactive">
            <div class="row megamenu__wrapper">
              <li class="megamenu__col col-lg-2">
                <a href="#" class="megamenu__header">hello</a>
                <ul class="megamenu__items">
                  <li class="megamenu__item"><a href="#">FXTM </a></li>
                  <li class="megamenu__item"><a href="#">OctaFX</a></li>
                  <li class="megamenu__item"><a href="#">FXCM </a> </li>
                  <li class="megamenu__item"><a href="#">Hot Forex</a></li>
                  <li class="megamenu__item"><a href="#">FX Pro</a></li>
                </ul>
              </li>
              <li class="megamenu__col col-lg-2">
                <a href="#" class="megamenu__header">Most Popular</a>
                <ul class="megamenu__items">
                  <li class="megamenu__item"><a href="#">FXTM</a></li>
                  <li class="megamenu__item"><a href="#">OctaFX</a></li>
                  <li class="megamenu__item"><a href="#">FXCM</a></li>
                  <li class="megamenu__item"><a href="#">Hot Forex</a></li>
                  <li class="megamenu__item"><a href="#">FX Pro</a></li>
                  <li class="megamenu__item"><a href="#">FX Choice</a></li>
                </ul>
              </li>
              <li class="megamenu__col col-lg-2">
                <a href="#" class="megamenu__header">Most Recent</a>
                <ul class="megamenu__items">
                  <li class="megamenu__item"><a href="#">Onada</a></li>
                  <li class="megamenu__item"><a href="#">MyTrade</a></li>
                  <li class="megamenu__item"><a href="#">Capital Index</a></li>
                </ul>
              </li>
              <li class="megamenu__col col-lg-2">
                <a href="#" class="megamenu__header">Highest Rated</a>
                <ul class="megamenu__items">
                  <li class="megamenu__item"><a href="#">Saxo Bank</a></li>
                  <li class="megamenu__item"><a href="#">Ayonodo</a></li>
                  <li class="megamenu__item"><a href="#">STO</a></li>
                  <li class="megamenu__item"><a href="#">GMG</a></li>
                  <li class="megamenu__item"><a href="#">Infinox</a></li>
                  <li class="megamenu__item"><a href="#">MyTrade</a></li>
                </ul>
              </li>
              <li class="megamenu__col col-lg-2">
                <a href="#" class="megamenu__header">Most Recent</a>
                <ul class="megamenu__items">
                  <li class="megamenu__item"><a href="">Onada</a></li>
                  <li class="megamenu__item"><a href="">MyTrade</a></li>
                  <li class="megamenu__item"><a href="">Capital Index</a></li>
                  <li class="megamenu__item"><a href="">Spread FX</a></li>
                  <li class="megamenu__item"><a href="">CommSec</a></li>
                  <li class="megamenu__item"><a href="">Colmex</a></li>
                </ul>
              </li>
              <li class="megamenu__col col-lg-2">
                <a href="#" class="megamenu__header">Highest Rated</a>
                <ul class="megamenu__items">
                  <li class="megamenu__item"><a href="#">Saxo Bank</a></li>
                  <li class="megamenu__item"><a href="#">Ayonodo</a></li>
                </ul>
              </li>
            </div>
          </ul>
        </li>
        <li class="nav__item"><a data-nav-link href="/price-pairings.php" class="nav__link nav__link--inactive">Prices & Pairings</a></li>
        <li class="nav__item"><a data-nav-link href=# class="nav__link nav__link--inactive">News </a></li>
        <li class="nav__item"><a data-nav-link href=# class="nav__link nav__link--inactive">Analysis </a></li>
        <li class="nav__item"><a data-nav-link href=# class="nav__link nav__link--inactive">Reviews </a></li>
        <li class="nav__item"><a data-nav-link href=# class="nav__link nav__link--inactive">Education </a></li>
        <li class="nav__item"><a data-nav-link href=# class="nav__link nav__link--inactive">Tools </a></li>
        <li class="nav__item"><a data-nav-link href=# class="nav__link nav__link--inactive">Webinars </a></li>
      </ul>
    </div>
  </div>
</nav>
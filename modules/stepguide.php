<section class="module stepguide">
  <div class="container">
    <h2 class="module__title">Step By Step guide</h2>
    <div class="row">
      <div class="col-lg-4 stepguide__step">
        <div class="stepguide__icons">
          <div class="stepguide__number">1</div>
        </div>
        <h3 class="stepguide__header">Lorem ipsum dolor sit amet consectetur adipisicing.</h3>
        <p class="stepguide__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos nesciunt nisi exercitationem? Totam nesciunt aliquam quisquam ad quaerat labore reiciendis quam quas distinctio libero, quibusdam unde, nam commodi eum! Odit!</p>
      </div>
      <div class="col-lg-4 stepguide__step">
        <div class="stepguide__icons">
          <div class="stepguide__number">2</div>
        </div>
        <h3 class="stepguide__header">Lorem ipsum dolor sit amet consectetur adipisicing.</h3>
        <p class="stepguide__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos nesciunt nisi exercitationem? Totam nesciunt aliquam quisquam ad quaerat labore reiciendis quam quas distinctio libero, quibusdam unde, nam commodi eum! Odit!</p>
      </div>
      <div class="col-lg-4 stepguide__step">
        <div class="stepguide__icons">
          <div class="stepguide__number">3</div>
        </div>
        <h3 class="stepguide__header">Lorem ipsum dolor sit amet consectetur adipisicing.</h3>
        <p class="stepguide__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos nesciunt nisi exercitationem? Totam nesciunt aliquam quisquam ad quaerat labore reiciendis quam quas distinctio libero, quibusdam unde, nam commodi eum! Odit!</p>
      </div>
    </div>
    <div class="stepguide__button">
      <a href="" class="btn btn--theme-b">Amazing Robot!</a>
    </div>
  </div>
</section>
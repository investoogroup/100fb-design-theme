<section class="module procons">
  <div class="container">
    <h2 class="module__title">Which type of broker should I trade with?</h2>
    <div class="row">
      <div class="col-lg-6">
        <h3 class="procons__header procons__header--pro">Pros:</h3>
        <ul class="procons__list procons__list--pro">
          <li class="procons__item">
            <img src="./assets/media/img/tick.png" alt="" class="procons__icon">
            <span class="procons__item-inner">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus cupiditate quod doloribus quas delectus</span>
          </li>
          <li class="procons__item">
            <img src="./assets/media/img/tick.png" alt="" class="procons__icon">
            <span class="procons__item-inner">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus cupiditate quod doloribus quas delectus</span>
          </li>
          <li class="procons__item">
            <img src="./assets/media/img/tick.png" alt="" class="procons__icon">
            <span class="procons__item-inner">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus cupiditate quod doloribus quas delectus</span>
          </li>
          <li class="procons__item">
            <img src="./assets/media/img/tick.png" alt="" class="procons__icon">
            <span class="procons__item-inner">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus cupiditate quod doloribus quas delectus</span>
          </li>
        </ul>
      </div>
      <div class="col-lg-6">
        <h3 class="procons__header procons__header--con">Cons:</h3>
        <ul class="procons__list procons__list--con">
          <li class="procons__item">
            <img src="./assets/media/img/cross.png" alt="" class="procons__icon">
            <span class="procons__item-inner">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus cupiditate quod doloribus quas delectus</span>
          </li>
          <li class="procons__item">
            <img src="./assets/media/img/cross.png" alt="" class="procons__icon">
            <span class="procons__item-inner">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus cupiditate quod doloribus quas delectus</span>
          </li>
        </ul>
      </div>
    </div>
    <div class="procons__button">
      <a href="" class="btn btn--theme-b">Sign Me Up!</a>
    </div>
  </div>
</section>
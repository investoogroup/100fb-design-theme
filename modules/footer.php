<footer class="footer">
  <div class="container container--wide">
    <div class="footer__main">
      <div class="footer__col">
        <div class="footer__header">Navigation</div>
        <ul class="footer__links">
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Broker Reviews</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
        </ul>
      </div>
      <div class="footer__col">
        <div class="footer__header">Navigation</div>
        <ul class="footer__links">
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Broker Reviews</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
        </ul>
      </div>
      <div class="footer__col">
        <div class="footer__header">Navigation</div>
        <ul class="footer__links">
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Broker Reviews</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
        </ul>
      </div>
      <div class="footer__col">
        <div class="footer__header">Navigation</div>
        <ul class="footer__links">
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Broker Reviews</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__link" href="#" class="">Home</a></li>
          <li class="footer__item"><a class="footer__highlighted-link" href="#" class="">Advertise With Us</a></li>
        </ul>
      </div>

      <div class="footer__col">
       <img class="footer__logo" src="./assets/media/logo.png">
      </div>
    </div>
    <div class="footer__disclaimer">
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis quos eveniet, deserunt unde totam error sequi est praesentium ea! Modi, nesciunt aliquid asperiores sed fuga quas nisi error ipsam quos? Lorem ipsum dolor sit amet consectetur adipisicing elit. A dolores perferendis, recusandae veritatis obcaecati eaque voluptas optio nihil, sunt, perspiciatis necessitatibus cupiditate consequatur iusto rem adipisci ipsam praesentium modi ab?</p>
    </div>
  </div>
</footer>